#ifndef IK_H
#define IK_H
#include <stdio.h>//TODO: Reimplement sprintf
#include <stdint.h>

#define global static
#define internal static
#define local_persist static

#ifdef IK_DEBUG
#define ASSERT(expression) \
    if(!(expression)) {int* a = 0; *a = 0;}
#else
#define ASSERT(expression)
#endif

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef i32 b32;
typedef float f32;
typedef double f64;

extern b32 global_running;

enum PlatformEventType : u32
{
    Event_NotAvailable = 0,
    Event_Quit,
    Event_Keyboard,
};
struct PlatformKeyboardEvent
{
    PlatformEventType type;
    b32 is_down;
    b32 is_repeating;
    b32 alt_was_down;
    u32 key;
};
union PlatformEvent
{
    PlatformEventType type;
    PlatformKeyboardEvent keyboard;
};

//ik_win32.h
#if defined(IK_WIN32) || defined(IK_WIN32_IMPLEMENT)

b32 win32_process_event(PlatformEvent* event);

struct Win32InitResult
{
    b32 success;
    HWND window;
};
Win32InitResult win32_init(HINSTANCE instance, const char* window_name,
                           i32 window_width, i32 window_height);

#endif//IK_WIN32


#ifdef IK_WIN32_IMPLEMENT

b32 global_running;

internal
void win32_print_last_error_code()
{
    DWORD error_code = GetLastError();
    char buf[512] = {};
    sprintf(buf, "Win32 Error Code: %d\n", error_code);
    OutputDebugStringA(buf);
}

//TODO: Combine this function with win32_main_callback
b32 win32_process_event(PlatformEvent* event)
{
    *event = {};
    b32 result = false;
    MSG message;

    if(PeekMessageA(&message, 0, 0, 0, PM_REMOVE))
    {
        WPARAM wparam = message.wParam;
        LPARAM lparam = message.lParam;
        switch(message.message)
        {
            case WM_DESTROY:
            case WM_CLOSE:
            case WM_QUIT:
            {
                event->type = Event_Quit;
            } break;

            case WM_SYSKEYDOWN:
            case WM_SYSKEYUP:
            case WM_KEYDOWN:
            case WM_KEYUP:
            {
                event->type = Event_Keyboard;

                u32 vk_code = (u32)wparam;
                b32 was_down = ((lparam & (1 << 30)) != 0);
                event->keyboard.is_down = ((lparam & (1 << 31)) == 0);
                event->keyboard.alt_was_down = ((lparam & (1 << 29)) != 0);
                if(event->keyboard.is_down)
                {
                    if(was_down)
                    {
                        event->keyboard.is_repeating = true;
                    }
                }
                else
                {
                    if(was_down)
                    {
                    }
                    else
                    {
                        event->keyboard.is_repeating = true;
                    }
                }
                event->keyboard.key = vk_code;
            } break;

            default:
            {
                TranslateMessage(&message);
                DispatchMessageA(&message);
            } break;
        }

        result = true;
    }

    return result;
}

internal
LRESULT CALLBACK win32_main_window_callback(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
    LRESULT result = 0;
    switch(message)
    {
        case WM_SIZE:
        {
        } break;

        case WM_DESTROY:
        {
            global_running = false;
        } break;

        case WM_CLOSE:
        {
            global_running = false;
        } break;

        case WM_ACTIVATEAPP:
        {
        } break;

        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
        case WM_KEYDOWN:
        case WM_KEYUP:
        {
            ASSERT(!"Keyboard input came in through a non-dispatch message!");
        } break;

        #if 0
        case WM_PAINT:
        {
            PAINTSTRUCT paint;
            HDC device_context = BeginPaint(window, &paint);

            Win32WindowDimension dimension = win32_get_window_dimension(window);

            win32_display_buffer_in_window(&global_back_buffer,
                                       device_context,
                                       dimension.width, dimension.height);
            EndPaint(window, &paint);
        } break;
        #endif

        default:
        {
            result = DefWindowProcA(window, message, wparam, lparam);
        } break;
    }

    return result;
}

Win32InitResult win32_init(HINSTANCE instance, const char* window_name,
                           i32 window_width, i32 window_height)
{
    Win32InitResult result = {};

    WNDCLASSA window_class = {};
    window_class.style = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    window_class.lpfnWndProc = win32_main_window_callback;
    window_class.hInstance = instance;
    //window_class.hIcon = ;
    //window_class.hCursor = ;
    //window_class.hbrBackground = ;
    window_class.lpszClassName = "ik_window_class";

    win32_print_last_error_code();

    if(RegisterClassA(&window_class))
    {
        HWND window = CreateWindowA(window_class.lpszClassName,
                                    window_name,
                                    WS_OVERLAPPEDWINDOW|WS_VISIBLE,
                                    CW_USEDEFAULT,
                                    CW_USEDEFAULT,
                                    window_width,
                                    window_height,
                                    NULL,
                                    NULL,
                                    instance,
                                    NULL);
        if(window)
        {
            result.success = true;
            result.window = window;
        }
        else
        {
            win32_print_last_error_code();
        }
    }
    else
    {
        win32_print_last_error_code();
    }

    return result;
}

#endif//IK_WIN32_IMPLEMENT

//ik_gl.h
#if defined(IK_GL) || defined(IK_GL_IMPLEMENT)
#ifdef _WIN32

// This loader code is based on Bink GL extension loader

// Acquired from https://www.khronos.org/registry/OpenGL/api/GL/glext.h
#define GL_ARRAY_BUFFER                   0x8892
#define GL_ELEMENT_ARRAY_BUFFER           0x8893
#define GL_VERTEX_SHADER                  0x8B31
#define GL_FRAGMENT_SHADER                0x8B30
#define GL_COMPILE_STATUS                 0x8B81
#define GL_LINK_STATUS                    0x8B82

typedef char GLchar;
#include <stddef.h>
typedef ptrdiff_t GLsizeiptr;
typedef ptrdiff_t GLintptr;

#include <windows.h>
#define GLDECL WINAPI

#include <gl/gl.h>

#define IK_GL_LIST \
    GLE(GLuint, CreateShader,            GLenum type) \
    GLE(void,   ShaderSource,            GLuint shader, GLsizei count, const GLchar* const*string, const GLint* length) \
    GLE(void,   CompileShader,           GLuint shader) \
    GLE(void,   GetShaderiv,             GLuint shader, GLenum pname, GLint* params) \
    GLE(void,   GetShaderInfoLog,        GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) \
    GLE(GLuint, CreateProgram,           void) \
    GLE(void,   AttachShader,            GLuint program, GLuint shader) \
    GLE(void,   LinkProgram,             GLuint program) \
    GLE(void,   GetProgramiv,            GLuint program, GLenum pname, GLint *params) \
    GLE(void,   DeleteShader,            GLuint shader) \
    GLE(void,   GenVertexArrays,         GLsizei n, GLuint *arrays) \
    GLE(void,   GenBuffers,              GLsizei n, GLuint *buffers) \
    GLE(void,   BindVertexArray,         GLuint array) \
    GLE(void,   BindBuffer,              GLenum target, GLuint buffer) \
    GLE(void,   BufferData,              GLenum target, GLsizeiptr size, const void *data, GLenum usage) \
    GLE(void,   VertexAttribPointer,     GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer) \
    GLE(void,   EnableVertexAttribArray, GLuint index) \
    GLE(void,   UseProgram,              GLuint program)

#define GLE(ret, name, ...) typedef ret GLDECL name##proc(__VA_ARGS__); \
    extern name##proc* gl##name; 
IK_GL_LIST
#undef GLE

b32 opengl_load_extensions();
b32 opengl_init(HWND window);

#else
#error "IK_GL: This platform is not supported yet!"
#endif//_WIN32

#endif//IK_GL
#ifdef IK_GL_IMPLEMENT
#ifdef _WIN32

#define GLE(ret, name, ...) typedef ret GLDECL name##proc(__VA_ARGS__); \
    name##proc* gl##name; 
IK_GL_LIST
#undef GLE

b32 opengl_load_extensions()
{
    b32 result = false;
    HMODULE opengl_dll = LoadLibraryA("opengl32.dll");
    if(opengl_dll)
    {
        typedef PROC WINAPI wglGetProcAddressproc(LPCSTR);
        wglGetProcAddressproc* wglGetProcAddress =
            (wglGetProcAddressproc*)GetProcAddress(opengl_dll, "wglGetProcAddress");

        result = true;

        #define GLE(ret, name, ...) \
            gl##name = (name##proc*)wglGetProcAddress("gl" #name); \
            if(!gl##name) \
            { \
                win32_print_last_error_code(); \
                OutputDebugStringA("Function gl" #name " couldn't be loaded from opengl32.dll!\n"); \
                result = false; \
            }
        IK_GL_LIST
        #undef GLE
    }
    else
    {
        OutputDebugStringA("opengl32.dll not found!\n");
    }

    return result;
}

b32 opengl_init(HWND window)
{
    b32 result = false;
    PIXELFORMATDESCRIPTOR pfd =
    {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
        PFD_TYPE_RGBA,            //The kind of framebuffer. RGBA or palette.
        32,                        //Colordepth of the framebuffer.
        0, 0, 0, 0, 0, 0,
        0,
        0,
        0,
        0, 0, 0, 0,
        24,                        //Number of bits for the depthbuffer
        8,                        //Number of bits for the stencilbuffer
        0,                        //Number of Aux buffers in the framebuffer.
        PFD_MAIN_PLANE,
        0,
        0, 0, 0
    };

    HDC dc = GetDC(window);
    int pixel_format = ChoosePixelFormat(dc, &pfd);
    if(pixel_format && (SetPixelFormat(dc, pixel_format, &pfd) == TRUE))
    {
        HGLRC rc = wglCreateContext(dc);
        if(wglMakeCurrent(dc, rc))
        {
            result = true;
        }
    }
    ReleaseDC(window, dc);

    return result;
}

#endif//_WIN32
#endif//IK_GL_IMPLEMENT

#endif//IK_H